class HomeController < ApplicationController
  def index
  	if current_user
  		redirect_to videos_path
  	end
  end
end
