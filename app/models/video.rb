class Video < ActiveRecord::Base
	validates :wistia, presence: true
    validates :description, presence: true,
                            length: { minimum: 10, maximum: 400 }
    validates :title, presence: true,
                      length: { minimum: 4, maximum: 20 }
    

	def next
       Video.where("id > ?", id).first
	end

	def prev
       Video.where("id < ?", id).last
	end
end
